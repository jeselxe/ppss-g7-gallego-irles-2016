/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio4;

import java.util.Arrays;
import java.util.Collection;
import org.easymock.EasyMock;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runners.Parameterized;

/**
 *
 * @author ppss
 */
public class ReservaMockTest {
    
    Reserva reserva;
    
    String login;
    String password;
    String socio;
    String[] isbns;
    boolean permiso;
    boolean db;
    String expected;
    
    @Parameterized.Parameters(name = "C{index}: realizaReserva({0}, {1}, {2}, {4}) = {5}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
            {"xxxx", "xxxx", "Luis", false, true, new String[] { "11111" }, "ERROR de permisos; "},
            {"ppss", "ppss", "Luis", true, true, new String[] {"33333"}, "ISBN invalido:33333; "},
            {"ppss", "ppss", "Pepe", true, true, new String[] {"11111"}, "SOCIO invalido; "},
            {"ppss", "ppss", "Luis", true, false, new String[] {"11111"}, "CONEXION invalida; "}
        });
    }
    
    public ReservaMockTest(String login, String password, String socio, boolean permiso, boolean db, String[] isbns, String expected) {
        this.login = login;
        this.password = password;
        this.socio = socio;
        this.isbns = isbns;
        this.permiso = permiso;
        this.db = db;
        this.expected = expected;
    }
    
    @Before
    public void setUp() {
    }
    

    /**
     * Test of realizaReserva method, of class Reserva.
     */
    @Test
    public void testRealizaReserva() {
        reserva = EasyMock.createMockBuilder(Reserva.class).addMockedMethods("compruebaPermisos", "getFactoriaBOs").createMock();
        
        FactoriaBOs factoriaMock = EasyMock.createMock(FactoriaBOs.class);
        IOperacionBO operacionMock = EasyMock.createMock(Operacion.class);
        
        EasyMock.expect(operacionMock.operacionReserva(socio, login)).andThrow();
        EasyMock.expect(factoriaMock.getOperacionBO()).andReturn(operacionMock);
        EasyMock.expect(reserva.compruebaPermisos(login, password, Usuario.ALUMNO)).andReturn(permiso);
        EasyMock.expect(reserva.getFactoriaBOs()).andReturn(factoriaMock);
        
        try {
            reserva.realizaReserva(login, password, socio, isbns);
            fail("Exception expected");
        } catch (Exception ex) {
            assertEquals(expected, ex.getMessage());
        }
    }
}
