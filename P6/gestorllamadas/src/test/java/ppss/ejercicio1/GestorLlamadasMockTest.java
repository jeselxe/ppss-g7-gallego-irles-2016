package ppss.ejercicio1;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ppss
 */
public class GestorLlamadasMockTest {
    
    public GestorLlamadasMockTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testC1() {
        
        int minutos = 10;
        int hora = 15;
        double expResult = 208;
        
        Calendario mock = EasyMock.createMock(Calendario.class);
        EasyMock.expect(mock.getHoraActual()).andReturn(hora);
        
        GestorLlamadasTestable gestor = new GestorLlamadasTestable();
        gestor.setCalendario(mock);
        EasyMock.replay(mock);
        
        double result = gestor.calculaConsumo(minutos);
        assertEquals(expResult, result, 0.0);
        
        EasyMock.verify(mock);
    }
    
    @Test
    public void testC2() {
        int minutos = 10;
        int hora = 2;
        double expResult = 105;
        
        Calendario mock = EasyMock.createMock(Calendario.class);
        EasyMock.expect(mock.getHoraActual()).andReturn(hora);
        
        GestorLlamadasTestable gestor = new GestorLlamadasTestable();
        gestor.setCalendario(mock);
        EasyMock.replay(mock);
        
        double result = gestor.calculaConsumo(minutos);
        assertEquals(expResult, result, 0.0);
        
        EasyMock.verify(mock);
    }
}
