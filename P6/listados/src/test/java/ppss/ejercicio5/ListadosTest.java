/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio5;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.easymock.EasyMock;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ppss
 */
public class ListadosTest {
    
    public ListadosTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of porApellidos method, of class Listados.
     */
    @Test
    public void testPorApellidos() {
        String tableName = "alumnos";
        String sql = "SELECT apellido1, apellido2, nombre FROM " + tableName;
        Listados instance = new Listados();
        String expResult = "Garcia, Molina, Ana\nLacalle, Verna, Jose Luis\n";
        
        Connection con = EasyMock.createMock(Connection.class);
        Statement stm = EasyMock.createMock(Statement.class);
        ResultSet rs = EasyMock.createMock(ResultSet.class);
        
        try {
            EasyMock.expect(rs.next()).andReturn(true).times(2).andReturn(false).times(1);
            EasyMock.expect(rs.getString("apellido1")).andReturn("Garcia").times(1).andReturn("Lacalle").times(1);
            EasyMock.expect(rs.getString("apellido2")).andReturn("Molina").times(1).andReturn("Verna").times(1);
            EasyMock.expect(rs.getString("nombre")).andReturn("Ana").times(1).andReturn("Jose Luis").times(1);
            EasyMock.expect(stm.executeQuery(sql)).andReturn(rs);
            EasyMock.expect(con.createStatement()).andReturn(stm);
           
            EasyMock.replay(con, stm, rs);
            
            String result = instance.porApellidos(con, tableName);
            assertEquals(expResult, result);
            
            EasyMock.verify(con, stm, rs);
        } catch (SQLException ex) {
            fail("Not exception expected");
        }
    }
    
    @Test(expected = SQLException.class)
    public void testExcepcion() throws SQLException {
        String tableName = "alumnos";
        String sql = "SELECT apellido1, apellido2, nombre FROM " + tableName;
        Listados instance = new Listados();
        
        Connection con = EasyMock.createMock(Connection.class);
        Statement stm = EasyMock.createMock(Statement.class);
        ResultSet rs = EasyMock.createMock(ResultSet.class);
        
        
        EasyMock.expect(rs.next()).andThrow(new SQLException());
        EasyMock.expect(stm.executeQuery(sql)).andReturn(rs);
        EasyMock.expect(con.createStatement()).andReturn(stm);

        EasyMock.replay(con, stm, rs);

        String result = instance.porApellidos(con, tableName);

        EasyMock.verify(con, stm, rs);
    }
    
}
