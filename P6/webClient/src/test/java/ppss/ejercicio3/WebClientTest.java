/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio3;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.easymock.EasyMock;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ppss
 */
public class WebClientTest {
    
    public WebClientTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getContent method, of class WebClient.
     */
    @Test
    public void testC1() throws MalformedURLException, IOException {
        URL url = new URL("http://www.ua.es");
        boolean failConnection = false;
        String expected = "Funciona";
        
        InputStream is = new ByteArrayInputStream(expected.getBytes());
        
        WebClient webClient = EasyMock.createMockBuilder(WebClient.class).addMockedMethod("createHttpURLConnection").createMock();
        HttpURLConnection connection = EasyMock.createMock(HttpURLConnection.class);
        
        EasyMock.expect(connection.getInputStream()).andReturn(is);
        EasyMock.expect(webClient.createHttpURLConnection(url)).andReturn(connection);
        
        EasyMock.replay(webClient, connection);
        
        assertEquals(expected, webClient.getContent(url));
        
        EasyMock.verify(webClient, connection);
        

    }

    @Test
    public void testC2() throws MalformedURLException, IOException {
        URL url = new URL("http://www.ua.es");
        boolean failConnection = false;
        String expected = null;
        
        WebClient webClient = EasyMock.createMockBuilder(WebClient.class).addMockedMethod("createHttpURLConnection").createMock();
        HttpURLConnection connection = EasyMock.createMock(HttpURLConnection.class);
        
        EasyMock.expect(connection.getInputStream()).andThrow(new IOException());
        EasyMock.expect(webClient.createHttpURLConnection(url)).andReturn(connection);
        
        EasyMock.replay(webClient, connection);
        
        assertNull(webClient.getContent(url));
        
        EasyMock.verify(webClient, connection);
    }
}
