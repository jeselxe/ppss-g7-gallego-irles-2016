/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio2;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.easymock.EasyMock;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import ppss.ejercicio2.excepciones.ClienteWebServiceException;

/**
 *
 * @author ppss
 */
public class PremioTest {
    
    Premio premio;
    
    public PremioTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of compruebaPremio method, of class Premio.
     */
    @Test
    public void testC1() {
        String expResult = "Premiado con pez de goma";
        premio = EasyMock.createMockBuilder(Premio.class).addMockedMethod("generaNumero").createMock();
        ClienteWebService mock = EasyMock.createMock(ClienteWebService.class);
        premio.cliente = mock;
        
        try {
            EasyMock.expect(mock.obtenerPremio()).andReturn("pez de goma");
        } catch (ClienteWebServiceException ex) {
            fail("No debe de saltar excepcion");
        }
        EasyMock.expect(premio.generaNumero()).andReturn(0.01F);
        
        EasyMock.replay(premio, mock);
        
        String result = premio.compruebaPremio();
        assertEquals(expResult, result);
        
        EasyMock.verify(premio, mock);
    }

    @Test
    public void testC2() {
        String expResult = "Premiado";
        premio = EasyMock.createMockBuilder(Premio.class).addMockedMethod("generaNumero").createMock();
        ClienteWebService mock = EasyMock.createMock(ClienteWebService.class);
        premio.cliente = mock;
        
        try {
            EasyMock.expect(mock.obtenerPremio()).andThrow(new ClienteWebServiceException());
        } catch (ClienteWebServiceException ex) {
            fail("No debe de saltar excepcion");
        }
        EasyMock.expect(premio.generaNumero()).andReturn(0.01F);
        
        EasyMock.replay(premio, mock);
        
        String result = premio.compruebaPremio();
        assertEquals(expResult, result);
        
        EasyMock.verify(premio, mock);
    }
}
