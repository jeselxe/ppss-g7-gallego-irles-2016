/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio1;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ppss
 */
public class GestorLlamadasTest {
    
    public GestorLlamadasTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of calculaConsumo method, of class GestorLlamadas.
     */
    @Test
    public void testCalculaConsumoC1() {
        int minutos = 10;
        int hora = 15;
        double expResult = 208;
        
        GestorLlamadasTestable instance = new GestorLlamadasTestable();
        instance.setHora(hora); 
        double result = instance.calculaConsumo(minutos);
        
        assertEquals(expResult, result, 0.0);
    }
    
     /**
     * Test of calculaConsumo method, of class GestorLlamadas.
     */
    @Test
    public void testCalculaConsumoC2() {
        int minutos = 10;
        int hora = 22;
        double expResult = 105;
        
        GestorLlamadasTestable instance = new GestorLlamadasTestable();
        instance.setHora(hora);
        double result = instance.calculaConsumo(minutos);
        
        assertEquals(expResult, result, 0.0);
    }
    
}
