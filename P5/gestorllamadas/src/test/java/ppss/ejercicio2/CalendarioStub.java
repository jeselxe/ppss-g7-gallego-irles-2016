/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio2;

/**
 *
 * @author ppss
 */
public class CalendarioStub extends Calendario {

    int hora;

    public void setHora(int hora) {
        this.hora = hora;
    }
    
    
    @Override
    public int getHoraActual() {
        return hora;
    }
    
    
}
