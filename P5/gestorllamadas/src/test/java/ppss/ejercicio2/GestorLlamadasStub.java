/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio2;

/**
 *
 * @author ppss
 */
public class GestorLlamadasStub extends GestorLlamadas {

    Calendario calendario;

    public void setCalendario(Calendario calendario) {
        this.calendario = calendario;
    }
    
    @Override
    public Calendario getCalendario() {
        return calendario;
    }
    
}
