/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import ppss.ejercicio3.excepciones.ReservaException;

/**
 *
 * @author ppss
 */
@RunWith(Parameterized.class)
public class ReservaExcepcionesTest {
    
    Reserva reserva;
    
    String login;
    String password;
    String socio;
    String[] isbns;
    boolean db;
    String expected;

    @Parameterized.Parameters(name = "C{index}: realizaReserva({0}, {1}, {2}, {4}) = {5}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
            {"xxxx", "xxxx", "Luis", true, new String[] { "11111" }, "ERROR de permisos; "},
            {"ppss", "ppss", "Luis", true, new String[] {"33333"}, "ISBN invalido:33333; "},
            {"ppss", "ppss", "Pepe", true, new String[] {"11111"}, "SOCIO invalido; "},
            {"ppss", "ppss", "Luis", false, new String[] {"11111"}, "CONEXION invalida; "}
        });
    }

    public ReservaExcepcionesTest(String login, String password, String socio, boolean db, String[] isbns, String expected) {
        this.login = login;
        this.password = password;
        this.socio = socio;
        this.isbns = isbns;
        this.db = db;
        this.expected = expected;
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        reserva = new ReservaTestable();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of realizaReserva method, of class Reserva.
     */
    @Test
    public void testRealizaReserva() {
        OperacionStub operacion = new OperacionStub();
        operacion.setDbAcces(db);
        OperacionFactory of = new OperacionFactory();
        of.setOperacion(operacion);
        
        boolean excepcion = false;
        
        try {
            reserva.realizaReserva(login, password, socio, isbns);
        } catch (Exception ex) {
            excepcion = true;
            assertEquals(expected, ex.getMessage());
        }
        
        if (!excepcion) fail("Exception expected");
    }
    
}
