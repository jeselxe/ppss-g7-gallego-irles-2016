/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio3;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ppss
 */
public class ReservaTest {
    
    public ReservaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testC2() {
        String login = "ppss", 
               password = "ppss", 
               socio = "Luis";
        boolean db = true;
        String[] isbns = new String[] {"11111", "22222"};
        
        OperacionStub operacion = new OperacionStub();
        operacion.setDbAcces(db);
        OperacionFactory of = new OperacionFactory();
        of.setOperacion(operacion);
        
        Reserva reserva = new ReservaTestable();
        
        try {
            reserva.realizaReserva(login, password, socio, isbns);
        } catch (Exception ex) {
            fail("Not exception expected");
        }
        
        
    }
}
