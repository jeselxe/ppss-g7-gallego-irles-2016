/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio3;

/**
 *
 * @author ppss
 */
public class OperacionFactory {
    
    private static IOperacionBO operacion;

    public static void setOperacion(IOperacionBO operacion) {
        OperacionFactory.operacion = operacion;
    }
    
    
    public static IOperacionBO getOperacion() {
        if (operacion == null) {
            return new Operacion();
        }
        else {
            return operacion;
        }
    } 
}
