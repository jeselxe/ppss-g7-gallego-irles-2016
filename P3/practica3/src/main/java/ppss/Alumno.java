/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author ppss
 */
public class Alumno {
    public boolean validaNif (String nif) {
        if(nif.length()!=9 || nif==null) {
            return false;
        }
        
        String dni = nif.substring(0, 8);
        char letra = nif.charAt(8);
        
        int dniNumber;
        try {
            dniNumber = Integer.parseInt(dni);
        } catch (Exception e) {
            return false;
        }
        
        if (dniNumber < 0) {
            return false;
        }
        
        Pattern pattern = Pattern.compile("[0-9]{8,8}");
        
        Matcher matcher = pattern.matcher(dni);
        String letras = "TRWAGMYFPDXBNJZSQVHLCKE";
        
        long ldni = 0;
        try {
            ldni = Long.parseLong(dni);
        } catch (NumberFormatException e) {
            return false;
        }

        int indice = (int) (ldni % 23);
        char letraEsperada = letras.charAt(indice);

        return matcher.matches() && letra == letraEsperada;
    }
}
