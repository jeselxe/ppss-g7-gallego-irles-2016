/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss;

import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author ppss
 */
@RunWith(Parameterized.class)
@Category(ConParametrosTests.class)
public class TestAlumnoConParametros {
    
    String nif;
    boolean esperado;
    
    Alumno alumno;
    
    @Parameterized.Parameters(name = "C{index}: validaNif({0}) = {1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
            {"123", false},
            {"1234567AA", false},
            {"-12345678", false},
            {"00000000X", false},
            {"00000000T", true}
        });
    }
    
    public TestAlumnoConParametros(String nif, boolean esperado) {
        this.nif = nif;
        this.esperado = esperado;
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        alumno = new Alumno();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testValidaNif() {
        assertEquals(esperado, alumno.validaNif(nif));
    }
}
