/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss;

import java.util.Arrays;
import java.util.Collection;
import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author ppss
 */
@RunWith(Parameterized.class)
@Category(ConParametrosTests.class)
public class TestMatriculaConParametros {
    
    int edad;
    boolean familiaNumerosa;
    boolean repetidor;
    float esperado;
    
    Matricula matricula;
    
    public TestMatriculaConParametros(int edad, boolean familiaNumerosa, boolean repetidor, float esperado) {
        this.edad = edad;
        this.familiaNumerosa = familiaNumerosa;
        this.repetidor = repetidor;
        this.esperado = esperado;
    }
    
    @Parameterized.Parameters(name = "C{index}: calculaTasaMatricula({0}, {1}, {2}) = {3}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
            {20, false, true, 2000.00f},
            {20, false, false, 500.00f},
            {20, true, true, 250.00f},
            {60, false, true, 400.00f},
            {70, false, true, 250.00f}
        });
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        matricula = new Matricula();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testTasaMatricula() {
        assertEquals(esperado, matricula.calculaTasaMatricula(edad, familiaNumerosa, repetidor), 0.002f);
    }
}
