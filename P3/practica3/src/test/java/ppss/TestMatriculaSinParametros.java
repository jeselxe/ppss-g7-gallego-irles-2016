/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.experimental.categories.Category;

/**
 *
 * @author ppss
 */
@Category(SinParametrosTests.class)
public class TestMatriculaSinParametros {
    
    int edad;
    boolean familiaNumerosa;
    boolean repetidor;
    
    Matricula matricula;
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        matricula = new Matricula();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testC1() {
        edad = 20;
        familiaNumerosa = false;
        repetidor = true;
        
        float esperado = 2000.00f;
        float real = matricula.calculaTasaMatricula(edad, familiaNumerosa, repetidor);
        
        assertEquals(esperado, real,0.002f);
    }
    
    @Test
    public void testC2() {
        edad = 20;
        familiaNumerosa = true;
        repetidor = true;
        
        float esperado = 250.00f;
        float real = matricula.calculaTasaMatricula(edad, familiaNumerosa, repetidor);
        
        assertEquals(esperado, real,0.002f);
    }
    
    @Test
    public void testC3() {
        edad = 20;
        familiaNumerosa = false;
        repetidor = false;
        
        float esperado = 500.00f;
        float real = matricula.calculaTasaMatricula(edad, familiaNumerosa, repetidor);
        
        assertEquals(esperado, real,0.002f);
    }
    
    @Test
    public void testC4() {
        edad = 60;
        familiaNumerosa = false;
        repetidor = true;
        
        float esperado = 400.00f;
        float real = matricula.calculaTasaMatricula(edad, familiaNumerosa, repetidor);
        
        assertEquals(esperado, real,0.002f);
    }
    
    @Test
    public void testC5() {
        edad = 70;
        familiaNumerosa = false;
        repetidor = true;
        
        float esperado = 250.00f;
        float real = matricula.calculaTasaMatricula(edad, familiaNumerosa, repetidor);
        
        assertEquals(esperado, real,0.002f);
    }
    
  
}
