/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.experimental.categories.Category;

/**
 *
 * @author ppss
 */
@Category(SinParametrosTests.class)
public class TestAlumnoSinParametros {
    
    String nif;
    
    Alumno alumno;
    
    public TestAlumnoSinParametros() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        alumno = new Alumno();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testC1() {
        nif = "123";
        boolean esperado = false;
                
        assertEquals(esperado, alumno.validaNif(nif));
    }
    
    @Test
    public void testC2() {
        nif = "1234567AA";
        boolean esperado = false;
                
        assertEquals(esperado, alumno.validaNif(nif));        
    }
    
    @Test
    public void testC3() {
        nif = "-12345678";
        boolean esperado = false;
                
        assertEquals(esperado, alumno.validaNif(nif));        
    }
    
    @Test
    public void testC4() {
        nif = "00000000X";
        boolean esperado = false;
                
        assertEquals(esperado, alumno.validaNif(nif));        
    }
    
    @Test
    public void testC5() {
        nif = "00000000T";
        boolean esperado = true;
                
        assertEquals(esperado, alumno.validaNif(nif));        
    }
}
