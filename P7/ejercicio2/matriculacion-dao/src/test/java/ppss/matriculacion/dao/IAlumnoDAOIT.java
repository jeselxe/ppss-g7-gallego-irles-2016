/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.matriculacion.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import org.dbunit.Assertion;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.util.fileloader.DataFileLoader;
import org.dbunit.util.fileloader.FlatXmlDataFileLoader;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import ppss.matriculacion.to.AlumnoTO;

/**
 *
 * @author ppss
 */
public class IAlumnoDAOIT {
    
    private IDatabaseTester databaseTester;
    IAlumnoDAO _alumnoDAO;
    
    public IAlumnoDAOIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws Exception {
        databaseTester = new JdbcDatabaseTester("com.mysql.jdbc.Driver", "jdbc:mysql://localhost:3306/matriculacion", "root", "ppss");
        
        DataFileLoader loader = new FlatXmlDataFileLoader();
        IDataSet ids = loader.load("/tabla2.xml");
        
        databaseTester.setDataSet(ids);
        databaseTester.onSetup();
        
        _alumnoDAO = new FactoriaDAO().getAlumnoDAO();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testA1() throws DAOException, SQLException, DataSetException, Exception {
        AlumnoTO a = new AlumnoTO();
        a.setNif("33333333C");
        a.setNombre("Elena Aguirre Juarez");
         
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.YEAR, 1985);
        cal.set(Calendar.MONTH, 1); //Nota: en la clase Calendar, el primer mes es 0 cal.set(Calendar.DATE, 22);
        cal.set(Calendar.DATE, 22);
        a.setFechaNacimiento(cal.getTime());         
         
        _alumnoDAO.addAlumno(a);
         
        IDatabaseConnection conn = databaseTester.getConnection();
        
        IDataSet databaseDataSet = conn.createDataSet();
        ITable actualTable = databaseDataSet.getTable("alumnos");
         
        DataFileLoader loader = new FlatXmlDataFileLoader();
	IDataSet expectedDataSet = loader.load("/tabla3.xml");
	
        ITable expectedTable = expectedDataSet.getTable("alumnos");
        
        Assertion.assertEquals(expectedTable, actualTable);
    }
    
    @Test(expected = DAOException.class)
    public void testA2() throws DAOException, SQLException, DataSetException, Exception {
        AlumnoTO a = new AlumnoTO();
        a.setNif("11111111A");
        a.setNombre("Alfonso Ramirez Ruiz");
         
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.YEAR, 1982);
        cal.set(Calendar.MONTH, 1); //Nota: en la clase Calendar, el primer mes es 0 cal.set(Calendar.DATE, 22);
        cal.set(Calendar.DATE, 22);
        a.setFechaNacimiento(cal.getTime());         
         
        _alumnoDAO.addAlumno(a);
    }
    
    @Test(expected = DAOException.class)
    public void testA3() throws DAOException, SQLException, DataSetException, Exception {
        AlumnoTO a = new AlumnoTO();
        a.setNif("44444444D");
        a.setNombre(null);
         
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.YEAR, 1982);
        cal.set(Calendar.MONTH, 1); //Nota: en la clase Calendar, el primer mes es 0 cal.set(Calendar.DATE, 22);
        cal.set(Calendar.DATE, 22);
        a.setFechaNacimiento(cal.getTime());         
         
        _alumnoDAO.addAlumno(a);
    }
    
    @Test(expected = DAOException.class)
    public void testA4() throws DAOException, SQLException, DataSetException, Exception {        
         
        _alumnoDAO.addAlumno(null);
    }
    
    @Test(expected = DAOException.class)
    public void testA5() throws DAOException, SQLException, DataSetException, Exception {
        AlumnoTO a = new AlumnoTO();
        a.setNif(null);
        a.setNombre("Pedro Garcia Lopez");
         
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.YEAR, 1982);
        cal.set(Calendar.MONTH, 1); //Nota: en la clase Calendar, el primer mes es 0 cal.set(Calendar.DATE, 22);
        cal.set(Calendar.DATE, 22);
        a.setFechaNacimiento(cal.getTime());         
         
        _alumnoDAO.addAlumno(a);
    }
    
    @Test
    public void testB1() throws DAOException, Exception {
        _alumnoDAO.delAlumno("11111111A");
        
        IDatabaseConnection conn = databaseTester.getConnection();
        
        IDataSet databaseDataSet = conn.createDataSet();
        ITable actualTable = databaseDataSet.getTable("alumnos");
         
        DataFileLoader loader = new FlatXmlDataFileLoader();
	IDataSet expectedDataSet = loader.load("/tabla4.xml");
	
        ITable expectedTable = expectedDataSet.getTable("alumnos");
        
        Assertion.assertEquals(expectedTable, actualTable);
    }
    
    @Test(expected = DAOException.class)
    public void testB2() throws DAOException {
        _alumnoDAO.delAlumno("33333333C");
    }
    
    
}
