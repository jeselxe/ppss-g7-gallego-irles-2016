/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

/**
 *
 * @author ppss
 */
public class TestLoginPage {
    WebDriver driver;
    LoginPage poLogin;
    ManagerPage poManager;
    
    public TestLoginPage() {
    }
    
    @Before
    public void setUp() {
        driver = new FirefoxDriver();
        poLogin = PageFactory.initElements(driver, LoginPage.class);
    }

    @Test
    public void testLoginCorrect() {
        String loginPageTitle = poLogin.getPageTitle();
        Assert.assertTrue(loginPageTitle.toLowerCase().contains("guru99 bank"));
        poLogin.login("mngr36768", "mevUmYd");
        
        poManager = PageFactory.initElements(driver, ManagerPage.class);
        Assert.assertTrue(poManager.getHomePageDashboardUserName().toLowerCase().contains("manger id : mngr36768"));
        driver.close();
    }
    
    @Test
    public void testLoginIncorrect() {
        String loginPageTitle = poLogin.getPageTitle();
        Assert.assertTrue(loginPageTitle.toLowerCase().contains("guru99 bank"));
        poLogin.login("login", "incorrect");
        
        Alert alert = driver.switchTo().alert();
        String mensaje = alert.getText();
        
        assertEquals("User or Password is not valid", mensaje);
        
        alert.accept();
        
        driver.close();
    }
}
