/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

/**
 *
 * @author ppss
 */
public class TestNewClient {
    
    WebDriver driver;
    LoginPage poLogin;
    ManagerPage poManager;
    NewCustomerPage poNewCustomer;
    
    public TestNewClient() {
    }
    
    @Before
    public void setUp() {
        driver = new FirefoxDriver();
        poLogin = PageFactory.initElements(driver, LoginPage.class);
    }

     @Test
     public void testNewClient() {
        String loginPageTitle = poLogin.getPageTitle();
        Assert.assertTrue(loginPageTitle.toLowerCase().contains("guru99 bank"));
        
        poManager = poLogin.login("mngr36768", "mevUmYd");
        Assert.assertTrue(poManager.getHomePageDashboardUserName().toLowerCase().contains("manger id : mngr36768"));
        
        poNewCustomer = poManager.newCustomer();
        assertEquals("Add New Customer", poNewCustomer.getNewCustomerPagerHeading());
        
        String id = poNewCustomer.newCustomer("jgii", "12/04/1994", "Calle x", "Elche", "Alicante", "123456", "999999999", "jgii8@alu.ua.es", "123456");
        System.out.println(id);
        //assertTrue(poNewCustomer.getVerification().contains("Customer Registered Successfully!!!"));
        
        driver.close();
     }
}
