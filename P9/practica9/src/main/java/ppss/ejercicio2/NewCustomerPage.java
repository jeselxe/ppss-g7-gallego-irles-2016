/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 *
 * @author ppss
 */
public class NewCustomerPage {
    WebDriver driver;
    @FindBy(xpath = "/html/body/table/tbody/tr/td/table/tbody/tr[1]/td/p") WebElement heading;
    @FindBy(name = "name") WebElement name;
    @FindBy(name = "dob") WebElement birthDate;
    @FindBy(name = "addr") WebElement address;
    @FindBy(name = "city") WebElement city;
    @FindBy(name = "state") WebElement state;
    @FindBy(name = "pinno") WebElement pin;
    @FindBy(name = "telephoneno") WebElement phone;
    @FindBy(name = "emailid") WebElement email;
    @FindBy(name = "password") WebElement password;
    @FindBy(name = "sub") WebElement submit;
    @FindBy(name = "res") WebElement reset;

    public NewCustomerPage(WebDriver driver) {
        this.driver = driver;
    }

    
    public String getNewCustomerPagerHeading() {
        return heading.getText();
    }
    
    public String getVerification() {
        WebElement verify = driver.findElement(By.xpath("//table/tbody/tr/td/table/tbody/tr[4]/td[2]"));
        return verify.getText();
    }
    
    public String newCustomer(String name,String birth,String addr,String city,String state,String pin,String phone,String email,String passwd) {
        
        this.name.sendKeys(name);
        this.birthDate.sendKeys(birth);
        this.address.sendKeys(addr);
        this.city.sendKeys(city);
        this.state.sendKeys(state);
        this.pin.sendKeys(pin);
        this.phone.sendKeys(phone);
        this.email.sendKeys(email);
        this.password.sendKeys(passwd);
        
        this.submit.click();
        
        WebElement userid = driver.findElement(By.xpath("//table/tbody/tr/td/table/tbody/tr[4]/td[2]"));
        
        return userid.getText();
    }
    
    
}
